-- Ежедневные поступления по видам транспорта --
SELECT
	tt.name AS type, SUM(r.price * pa.amount) AS income
FROM Transport_On_Routes tor
	JOIN Routes r ON (r.id = tor.route_id)
	JOIN Transports t ON (t.id = tor.transport_id)
	JOIN Transport_Models tm ON (tm.id = t.model_id)
	JOIN Transport_Types tt ON (tt.id = tm.type_id)
	JOIN Passengers_Amount pa ON (pa.tor_id = tor_id)
GROUP BY tt.name

-- Протяженность пути по видам ТС --
WITH Route_Length AS (
	SELECT
		r.id, r.number AS route, d.name AS direction, SUM(rs.length) AS path
	FROM Routes r
		JOIN Route_Stops rs ON (rs.route_id = r.id)
		JOIN Directions d ON (d.id = r.direction_id)
	GROUP BY r.id, route, direction
)
SELECT DISTINCT
	tt.name AS type, rl.route, rl.direction, rl.path
FROM Route_Length rl
	JOIN Transport_On_routes tor on (tor.route_id = rl.id)
	JOIN Transports t ON (t.id = tor.transport_id)
	JOIN Transport_Models tm ON (tm.id = t.model_id)
	JOIN Transport_Types tt ON (tt.id = tm.type_id)
ORDER BY rl.path DESC

-- Время отправления по маршрутам и типам ТС
SELECT DISTINCT
	tor.id, tor.transport_id, tor.route_id, tor.depart
FROM Transport_On_Routes tor
	JOIN Transports t ON (t.id = tor.transport_id)
	JOIN Transport_Models tm ON (tm.id = t.model_id)
	JOIN Transport_Types tt ON (tt.id = tm.type_id)
	JOIN Passengers_Amount pa ON (pa.tor_id = tor_id)
ORDER BY tor.depart

-- Время в пути между пунктами --
WITH Routes_With_Stops AS (
	SELECT
		r.id
	FROM Routes r
		JOIN Route_Stops rs ON (rs.route_id = r.id)
	WHERE rs.name = 'ПАТП-2'
	INTERSECT
	SELECT
		r.id
	FROM Routes r
		JOIN Route_Stops rs ON (rs.route_id = r.id)
	WHERE rs.name = 'Авторынок'
),
Route_Length AS (
	SELECT
		rws.id, r.number AS route, d.name AS direction, SUM(rs.length) AS path
	FROM Routes_With_Stops rws
		JOIN Routes r ON (r.id = rws.id)
		JOIN Route_Stops rs ON (rs.route_id = r.id)
		JOIN Directions d ON (d.id = r.direction_id)
	GROUP BY rws.id, route, direction
)
SELECT DISTINCT
	tt.name AS type, rl.route, rl.direction, rl.path/tm.avg_speed*60 AS time
FROM Route_Length rl
	JOIN Transport_On_Routes tor on (tor.route_id = rl.id)
	JOIN Transports t ON (t.id = tor.transport_id)
	JOIN Transport_Models tm ON (tm.id = t.model_id)
	JOIN Transport_Types tt ON (tt.id = tm.type_id)
ORDER BY time
LIMIT 1