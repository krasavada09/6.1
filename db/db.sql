--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-06-04 21:10:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 203 (class 1259 OID 17870)
-- Name: directions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.directions (
    id integer NOT NULL,
    name character varying(20) NOT NULL
);


ALTER TABLE public.directions OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17868)
-- Name: directions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.directions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.directions_id_seq OWNER TO postgres;

--
-- TOC entry 2900 (class 0 OID 0)
-- Dependencies: 202
-- Name: directions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.directions_id_seq OWNED BY public.directions.id;


--
-- TOC entry 210 (class 1259 OID 25952)
-- Name: passengers_amount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.passengers_amount (
    tor_id integer NOT NULL,
    amount integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.passengers_amount OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 17891)
-- Name: route_stops; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.route_stops (
    id integer NOT NULL,
    stop_number integer NOT NULL,
    name character varying(30) NOT NULL,
    length double precision DEFAULT 0 NOT NULL,
    route_id integer NOT NULL
);


ALTER TABLE public.route_stops OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 17889)
-- Name: route_stops_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.route_stops_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.route_stops_id_seq OWNER TO postgres;

--
-- TOC entry 2901 (class 0 OID 0)
-- Dependencies: 206
-- Name: route_stops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.route_stops_id_seq OWNED BY public.route_stops.id;


--
-- TOC entry 205 (class 1259 OID 17878)
-- Name: routes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.routes (
    id integer NOT NULL,
    number character varying(5),
    direction_id integer NOT NULL,
    price money NOT NULL
);


ALTER TABLE public.routes OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 17876)
-- Name: routes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.routes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.routes_id_seq OWNER TO postgres;

--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 204
-- Name: routes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.routes_id_seq OWNED BY public.routes.id;


--
-- TOC entry 199 (class 1259 OID 17842)
-- Name: transport_models; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transport_models (
    id integer NOT NULL,
    type_id integer NOT NULL,
    name character varying(30) NOT NULL,
    avg_speed integer NOT NULL
);


ALTER TABLE public.transport_models OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 17840)
-- Name: transport_models_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transport_models_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transport_models_id_seq OWNER TO postgres;

--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 198
-- Name: transport_models_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transport_models_id_seq OWNED BY public.transport_models.id;


--
-- TOC entry 209 (class 1259 OID 17907)
-- Name: transport_on_routes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transport_on_routes (
    id integer NOT NULL,
    transport_id integer NOT NULL,
    route_id integer NOT NULL,
    depart time without time zone NOT NULL
);


ALTER TABLE public.transport_on_routes OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 17905)
-- Name: transport_on_rotes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transport_on_rotes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transport_on_rotes_id_seq OWNER TO postgres;

--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 208
-- Name: transport_on_rotes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transport_on_rotes_id_seq OWNED BY public.transport_on_routes.id;


--
-- TOC entry 197 (class 1259 OID 17834)
-- Name: transport_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transport_types (
    id integer NOT NULL,
    name character varying(30) NOT NULL
);


ALTER TABLE public.transport_types OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 17832)
-- Name: transport_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transport_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transport_types_id_seq OWNER TO postgres;

--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 196
-- Name: transport_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transport_types_id_seq OWNED BY public.transport_types.id;


--
-- TOC entry 201 (class 1259 OID 17855)
-- Name: transports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transports (
    id integer NOT NULL,
    number character varying(10) NOT NULL,
    model_id integer NOT NULL
);


ALTER TABLE public.transports OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 17853)
-- Name: transports_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transports_id_seq OWNER TO postgres;

--
-- TOC entry 2906 (class 0 OID 0)
-- Dependencies: 200
-- Name: transports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transports_id_seq OWNED BY public.transports.id;


--
-- TOC entry 2728 (class 2604 OID 17873)
-- Name: directions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.directions ALTER COLUMN id SET DEFAULT nextval('public.directions_id_seq'::regclass);


--
-- TOC entry 2730 (class 2604 OID 17894)
-- Name: route_stops id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.route_stops ALTER COLUMN id SET DEFAULT nextval('public.route_stops_id_seq'::regclass);


--
-- TOC entry 2729 (class 2604 OID 17881)
-- Name: routes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.routes ALTER COLUMN id SET DEFAULT nextval('public.routes_id_seq'::regclass);


--
-- TOC entry 2726 (class 2604 OID 17845)
-- Name: transport_models id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_models ALTER COLUMN id SET DEFAULT nextval('public.transport_models_id_seq'::regclass);


--
-- TOC entry 2732 (class 2604 OID 17910)
-- Name: transport_on_routes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_on_routes ALTER COLUMN id SET DEFAULT nextval('public.transport_on_rotes_id_seq'::regclass);


--
-- TOC entry 2725 (class 2604 OID 17837)
-- Name: transport_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_types ALTER COLUMN id SET DEFAULT nextval('public.transport_types_id_seq'::regclass);


--
-- TOC entry 2727 (class 2604 OID 17858)
-- Name: transports id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transports ALTER COLUMN id SET DEFAULT nextval('public.transports_id_seq'::regclass);


--
-- TOC entry 2887 (class 0 OID 17870)
-- Dependencies: 203
-- Data for Name: directions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.directions (id, name) FROM stdin;
1	Прямое
2	Обратное
\.


--
-- TOC entry 2894 (class 0 OID 25952)
-- Dependencies: 210
-- Data for Name: passengers_amount; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.passengers_amount (tor_id, amount) FROM stdin;
1	50
2	50
3	50
4	50
5	50
6	50
7	50
8	50
9	50
10	50
11	50
12	50
13	50
14	50
15	50
16	50
17	50
18	50
19	50
20	50
21	50
22	50
23	50
24	50
25	50
26	50
27	50
28	50
29	50
30	50
31	50
32	50
33	50
34	50
35	50
36	50
37	50
38	50
39	50
40	50
41	50
42	50
49	25
50	25
51	25
52	25
53	25
54	25
55	25
56	25
57	25
58	25
59	25
60	25
61	25
62	25
63	25
64	25
65	25
66	25
67	25
68	25
69	25
70	25
71	25
72	25
73	75
74	75
75	75
76	75
77	75
78	75
79	75
80	75
81	75
82	75
83	75
84	75
85	100
86	100
87	100
88	100
89	100
90	100
91	100
92	100
\.


--
-- TOC entry 2891 (class 0 OID 17891)
-- Dependencies: 207
-- Data for Name: route_stops; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.route_stops (id, stop_number, name, length, route_id) FROM stdin;
1	1	ПАТП-2	0	13
2	2	Магазин Ливадд	0.469999999999999973	13
3	3	Управление соц. защиты	0.550000000000000044	13
4	4	Хоккейный корт	0.57999999999999996	13
5	5	Улица Пионерская	0.880000000000000004	13
6	6	Больничный комплекс	0.23000000000000001	13
7	7	ТК Империя-Т	0.409999999999999976	13
8	8	Нефтяной техникум	0.599999999999999978	13
9	9	Диско-клуб Мираж	0.409999999999999976	13
10	10	Рынок Сибирский Балаган	0.67000000000000004	13
11	11	Горвоенкомат	0.479999999999999982	13
12	12	Улица Мира, 96	0.589999999999999969	13
13	13	Улица Мира, 97	0.270000000000000018	13
14	14	Улица Профсоюзная, 9	0.429999999999999993	13
15	15	Улица Профсоюзная, 3	0.46000000000000002	13
16	16	Ромашка	0.729999999999999982	13
17	17	Авторынок	1.5	13
19	1	Авторынок	0	14
20	2	Ромашка	0.790000000000000036	14
21	3	Улица Профсоюзная, 3	0.699999999999999956	14
22	4	Улица Профсоюзная, 9	0.5	14
23	5	Улица Мира, 97	0.409999999999999976	14
24	6	Улица Мира, 96	0.260000000000000009	14
25	7	Горвоенкомат	0.599999999999999978	14
26	8	Рынок Сибирский Балаган	0.520000000000000018	14
27	9	Диско-клуб Мираж	0.560000000000000053	14
28	10	ТК Империя-Т	0.890000000000000013	14
29	11	Больничный комплекс	0.309999999999999998	14
30	12	Улица Пионерская	0.419999999999999984	14
31	13	Детская школа искусств	0.609999999999999987	14
32	14	Хоккейный корт	0.369999999999999996	14
33	15	Управление соц. защиты	0.560000000000000053	14
34	16	ПАТП-2	1	14
35	1	Авторынок	0	17
36	2	Ромашка	0.780000000000000027	17
37	3	Пожарная часть № 42	0.450000000000000011	17
38	4	Нижневартовские эл. сети	0.450000000000000011	17
39	5	10-й микрорайон	0.569999999999999951	17
40	6	Олимпия	0.520000000000000018	17
41	7	Диско-клуб Мираж	0.429999999999999993	17
42	8	Детская стомат. поликлиника	0.680000000000000049	17
43	9	Магазин Сибирь	0.520000000000000018	17
44	10	КДЦ Самотлор	0.560000000000000053	17
45	11	Рынок Центральный	0.569999999999999951	17
46	12	Самотлорнефтегаз	0.46000000000000002	17
47	13	Налоговая инспекция	0.729999999999999982	17
48	14	Управление соц. защиты	0.599999999999999978	17
49	15	ПАТП-2	1	17
50	1	ПАТП-2	0	18
51	2	Магазин Ливадд	0.599999999999999978	18
52	3	Управление соц. защиты	0.540000000000000036	18
53	4	Магазин Людмила	0.92000000000000004	18
54	5	Главпочтамт	0.819999999999999951	18
55	6	КДЦ Самотлор	0.419999999999999984	18
56	7	Магазин Сибирь	0.859999999999999987	18
57	8	Нефтяной техникум	0.790000000000000036	18
58	9	Диско-клуб Мираж	0.419999999999999984	18
59	10	Городская телефонная сеть	0.409999999999999976	18
60	11	Школа № 23	0.450000000000000011	18
61	12	Нижневартовские эл. сети	0.689999999999999947	18
62	13	Пожарная часть № 42	0.440000000000000002	18
63	14	Ромашка	0.57999999999999996	18
64	15	Авторынок	0.739999999999999991	18
\.


--
-- TOC entry 2889 (class 0 OID 17878)
-- Dependencies: 205
-- Data for Name: routes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.routes (id, number, direction_id, price) FROM stdin;
1	3	1	20.00
2	3	2	20.00
3	4	1	20.00
4	4	2	20.00
5	5	1	20.00
6	5	2	20.00
7	6	1	20.00
8	6	2	20.00
9	7	1	20.00
10	7	2	20.00
11	9	1	20.00
12	9	2	20.00
13	10	1	20.00
14	10	2	20.00
15	11	1	20.00
16	11	2	20.00
17	12	1	20.00
18	12	2	20.00
19	13	1	20.00
20	13	2	20.00
21	14	1	20.00
22	14	2	20.00
23	15	1	20.00
24	15	2	20.00
25	16	1	20.00
26	16	2	20.00
27	17	1	20.00
28	17	2	20.00
29	21	1	22.00
30	21	2	22.00
31	30	1	22.50
32	30	2	22.50
33	91	1	20.00
34	91	2	20.00
35	92	1	20.00
36	92	2	20.00
37	93	1	20.00
38	93	2	20.00
39	94	1	22.00
40	94	2	22.00
41	95	1	22.00
42	95	2	22.00
43	101	1	98.00
44	101	2	98.00
45	103	1	45.00
46	103	2	45.00
\.


--
-- TOC entry 2883 (class 0 OID 17842)
-- Dependencies: 199
-- Data for Name: transport_models; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transport_models (id, type_id, name, avg_speed) FROM stdin;
1	1	ЛиАЗ-5293	40
2	1	МАЗ-103	40
3	1	МАЗ-206	45
4	2	ГАЗель NEXT	45
5	2	ГАЗель Бизнес	45
6	2	Mercedes-Benz Sprinter	50
7	3	ЛиАЗ-5280	40
8	3	ElectroLAZ-12	40
9	4	71-403	40
10	4	71-154М	45
11	4	71-911E	45
12	5	81-740/741 «Русич»	70
13	5	81-760/761 «Ока»	70
\.


--
-- TOC entry 2893 (class 0 OID 17907)
-- Dependencies: 209
-- Data for Name: transport_on_routes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transport_on_routes (id, transport_id, route_id, depart) FROM stdin;
1	1	13	05:56:00
2	1	13	07:00:00
3	1	13	08:24:00
4	1	14	06:28:00
5	1	14	07:42:00
6	1	14	09:09:00
7	2	13	06:04:00
8	2	13	07:10:00
9	2	13	08:34:00
10	2	14	06:37:00
11	2	14	07:52:00
12	2	14	09:23:00
13	3	13	06:12:00
14	3	13	07:20:00
15	3	13	08:44:00
16	3	14	06:46:00
17	3	14	08:02:00
18	3	14	10:05:00
19	4	13	06:20:00
20	4	13	07:30:00
21	4	13	08:54:00
22	4	14	06:55:00
23	4	14	08:12:00
24	4	14	09:37:00
25	1	17	14:34:00
26	1	17	15:52:00
27	1	17	17:16:00
28	1	18	15:10:00
29	1	18	16:34:00
30	1	18	18:52:00
31	2	17	13:46:00
32	2	17	15:00:00
33	2	17	16:20:00
34	2	18	14:22:00
35	2	18	15:38:00
36	2	18	17:02:00
37	3	17	14:10:00
38	3	17	15:26:00
39	3	17	16:48:00
40	3	18	14:46:00
41	3	18	16:06:00
42	3	18	17:30:00
49	5	13	09:51:00
50	5	13	10:27:00
51	5	13	11:41:00
52	5	14	11:05:00
53	5	14	12:23:00
54	5	14	13:35:00
55	6	13	10:05:00
56	6	13	10:44:00
57	6	13	11:56:00
58	6	14	11:20:00
59	6	14	12:32:00
60	6	14	13:44:00
61	7	17	10:00:00
62	7	17	10:52:00
63	7	17	12:06:00
64	7	18	11:30:00
65	7	18	12:42:00
66	7	18	13:54:00
67	8	17	10:00:00
68	8	17	11:18:00
69	8	17	12:30:00
70	8	18	10:42:00
71	8	18	11:54:00
72	8	18	13:06:00
73	9	13	06:04:00
74	9	13	07:13:00
75	9	13	08:33:00
76	9	14	06:12:00
77	9	14	07:22:00
78	9	14	08:44:00
79	10	13	06:52:00
80	10	13	08:13:00
81	10	13	09:33:00
82	10	14	06:20:00
83	10	14	07:32:00
84	10	14	08:55:00
85	11	17	06:48:00
86	11	17	08:08:00
87	11	18	06:12:00
88	11	18	07:28:00
89	12	17	10:52:00
90	12	17	11:42:00
91	12	18	08:50:00
92	12	18	10:14:00
\.


--
-- TOC entry 2881 (class 0 OID 17834)
-- Dependencies: 197
-- Data for Name: transport_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transport_types (id, name) FROM stdin;
1	Автобус
2	Маршрутное такси
3	Троллейбус
4	Трамвай
5	Метро
\.


--
-- TOC entry 2885 (class 0 OID 17855)
-- Dependencies: 201
-- Data for Name: transports; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transports (id, number, model_id) FROM stdin;
1	АО 489 86	1
2	АТ 784 86	1
3	ВХ 718 186	3
4	УТ 178 186	3
5	МА 657 86	5
6	ЕК 415 86	5
7	ВМ 710 86	5
8	ЕТ 878 186	4
9	ТМ 455 86	7
10	ТК 414 86	8
11	БК 789 86	9
12	ВК 456 186	10
\.


--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 202
-- Name: directions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.directions_id_seq', 2, true);


--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 206
-- Name: route_stops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.route_stops_id_seq', 64, true);


--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 204
-- Name: routes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.routes_id_seq', 46, true);


--
-- TOC entry 2910 (class 0 OID 0)
-- Dependencies: 198
-- Name: transport_models_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transport_models_id_seq', 13, true);


--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 208
-- Name: transport_on_rotes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transport_on_rotes_id_seq', 92, true);


--
-- TOC entry 2912 (class 0 OID 0)
-- Dependencies: 196
-- Name: transport_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transport_types_id_seq', 5, true);


--
-- TOC entry 2913 (class 0 OID 0)
-- Dependencies: 200
-- Name: transports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transports_id_seq', 12, true);


--
-- TOC entry 2743 (class 2606 OID 17875)
-- Name: directions directions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.directions
    ADD CONSTRAINT directions_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 25957)
-- Name: passengers_amount passengers_amount_tor_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.passengers_amount
    ADD CONSTRAINT passengers_amount_tor_id_key UNIQUE (tor_id);


--
-- TOC entry 2747 (class 2606 OID 17897)
-- Name: route_stops route_stops_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.route_stops
    ADD CONSTRAINT route_stops_pkey PRIMARY KEY (id);


--
-- TOC entry 2745 (class 2606 OID 17883)
-- Name: routes routes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_pkey PRIMARY KEY (id);


--
-- TOC entry 2737 (class 2606 OID 17847)
-- Name: transport_models transport_models_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_models
    ADD CONSTRAINT transport_models_pkey PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 17912)
-- Name: transport_on_routes transport_on_rotes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_on_routes
    ADD CONSTRAINT transport_on_rotes_pkey PRIMARY KEY (id);


--
-- TOC entry 2735 (class 2606 OID 17839)
-- Name: transport_types transport_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_types
    ADD CONSTRAINT transport_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2739 (class 2606 OID 25951)
-- Name: transports transports_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transports
    ADD CONSTRAINT transports_number_key UNIQUE (number);


--
-- TOC entry 2741 (class 2606 OID 17860)
-- Name: transports transports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transports
    ADD CONSTRAINT transports_pkey PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 25958)
-- Name: passengers_amount passengers_amount_tor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.passengers_amount
    ADD CONSTRAINT passengers_amount_tor_id_fkey FOREIGN KEY (tor_id) REFERENCES public.transport_on_routes(id) ON DELETE CASCADE;


--
-- TOC entry 2755 (class 2606 OID 17900)
-- Name: route_stops route_stops_route_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.route_stops
    ADD CONSTRAINT route_stops_route_id_fkey FOREIGN KEY (route_id) REFERENCES public.routes(id) ON DELETE CASCADE;


--
-- TOC entry 2754 (class 2606 OID 17884)
-- Name: routes routes_direction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_direction_id_fkey FOREIGN KEY (direction_id) REFERENCES public.directions(id) ON DELETE RESTRICT;


--
-- TOC entry 2752 (class 2606 OID 17848)
-- Name: transport_models transport_models_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_models
    ADD CONSTRAINT transport_models_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.transport_types(id) ON DELETE CASCADE;


--
-- TOC entry 2757 (class 2606 OID 17918)
-- Name: transport_on_routes transport_on_rotes_route_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_on_routes
    ADD CONSTRAINT transport_on_rotes_route_id_fkey FOREIGN KEY (route_id) REFERENCES public.routes(id) ON DELETE CASCADE;


--
-- TOC entry 2756 (class 2606 OID 17913)
-- Name: transport_on_routes transport_on_rotes_transport_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport_on_routes
    ADD CONSTRAINT transport_on_rotes_transport_id_fkey FOREIGN KEY (transport_id) REFERENCES public.transports(id) ON DELETE RESTRICT;


--
-- TOC entry 2753 (class 2606 OID 17863)
-- Name: transports transports_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transports
    ADD CONSTRAINT transports_model_id_fkey FOREIGN KEY (model_id) REFERENCES public.transport_models(id) ON DELETE CASCADE;


-- Completed on 2020-06-04 21:10:39

--
-- PostgreSQL database dump complete
--

