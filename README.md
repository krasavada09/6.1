# Производственная практика - 6 семестр

## Файлы проекта

* Отчет.docx - Документация проекта
* db/db.sql - Дамп базы данных
* CityTransport - Исходные файлы C++
* build_MinGW_64_bit-Release/CityTransport.exe - Исполняемый файл программы
---

## Запуск программы
### Запуск базы данных
~~~
docker-compose up
~~~

### Запуск клиента
build_MinGW_64_bit-Release/CityTransport.exe

---
### Остановка базы данных
~~~
docker-compose down
~~~