#include "Widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QPushButton *btnPath = new QPushButton("Оптимальный маршрут");
    QPushButton *btnTime = new QPushButton("Среднее время ожидания");
    QPushButton *btnWays = new QPushButton("Маршруты по убыванию протяженности");
    QPushButton *btnIncome = new QPushButton("Поступления по видам транспорта");

    connect(btnPath, SIGNAL(clicked()), SLOT(path()));
    connect(btnTime, SIGNAL(clicked()), SLOT(time()));
    connect(btnWays, SIGNAL(clicked()), SLOT(ways()));
    connect(btnIncome, SIGNAL(clicked()), SLOT(income()));

    // Layout setup
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(btnPath);
    layout->addWidget(btnTime);
    layout->addWidget(btnWays);
    layout->addWidget(btnIncome);
    layout->setMargin(20);

    setLayout(layout);
}

void Widget::path()
{
    QSqlQuery query;
    query.exec("SELECT DISTINCT name FROM route_stops ORDER BY name");

    QStringList stopNames;
    while (query.next()) {
        stopNames.append(query.value("name").toString());
    }

    QInputDialog dlg1;
    dlg1.setWindowTitle("Введите значение");
    dlg1.setLabelText("Выберите первый пункт");
    dlg1.setComboBoxItems(stopNames);
    if (dlg1.exec() == QDialog::Rejected) return;

    QInputDialog dlg2;
    dlg2.setWindowTitle("Введите значение");
    dlg2.setLabelText("Выберите второй пункт");
    dlg2.setComboBoxItems(stopNames);
    if (dlg2.exec() == QDialog::Rejected) return;

    query.prepare("WITH Routes_With_Stops AS ( "
                  "SELECT "
                  "r.id "
                  "FROM Routes r "
                  "JOIN Route_Stops rs ON (rs.route_id = r.id) "
                  "WHERE rs.name = :name1 "
                  "INTERSECT "
                  "SELECT "
                  "r.id "
                  "FROM Routes r "
                  "JOIN Route_Stops rs ON (rs.route_id = r.id) "
                  "WHERE rs.name = :name2 "
                  "), "
                  "Route_Length AS ( "
                  "SELECT "
                  "rws.id, r.number AS route, d.name AS direction, "
                  "SUM(rs.length) AS path "
                  "FROM Routes_With_Stops rws "
                  "JOIN Routes r ON (r.id = rws.id) "
                  "JOIN Route_Stops rs ON (rs.route_id = r.id) "
                  "JOIN Directions d ON (d.id = r.direction_id) "
                  "GROUP BY rws.id, route, direction "
                  ") "
                  "SELECT DISTINCT "
                  "tt.name AS type, rl.route, rl.direction, "
                  "rl.path/tm.avg_speed*60 AS time "
                  "FROM Route_Length rl "
                  "JOIN Transport_On_Routes tor on (tor.route_id = rl.id) "
                  "JOIN Transports t ON (t.id = tor.transport_id) "
                  "JOIN Transport_Models tm ON (tm.id = t.model_id) "
                  "JOIN Transport_Types tt ON (tt.id = tm.type_id) "
                  "ORDER BY time "
                  "LIMIT 1"
                  );
    query.bindValue(":name1", dlg1.textValue());
    query.bindValue(":name2", dlg2.textValue());
    query.exec();

    if (query.size() < 1) {
        QMessageBox::warning(this, "Ошибка", "Маршрут не найден");
        return;
    }

    query.first();

    QMessageBox::information(this,
                             "Оптимальный маршрут",
                             "Тип ТС: <b>" +
                                query.value("type").toString() + "</b><br>" +
                             "Номер маршрута: <b>" +
                                query.value("route").toString() + "</b><br>"+
                             "Направление: <b>" +
                                query.value("direction").toString() + "</b><br>"+
                             "Время в пути: <b>" +
                                QString::number(query.value("time").toDouble(),
                                                'f', 0
                                                ) +
                                 " мин.</b>"
                             );
}

void Widget::time()
{
    QSqlQuery query;
    query.exec("SELECT DISTINCT "
               "tor.route_id AS id, r.number, d.name AS direction "
               "FROM Transport_On_Routes tor "
               "JOIN Routes r ON (r.id = tor.route_id) "
               "JOIN Directions d ON (d.id = r.direction_id) "
               "ORDER BY r.number, d.name"
               );

    QMap<int, QString> routes;
    while (query.next()) {
        routes.insert(query.value("id").toInt(),
                      query.value("number").toString() + " " +
                      query.value("direction").toString()
                      );
    }

    ComboBoxDialog dlg1;
    dlg1.setWindowTitle("Введите значение");
    dlg1.setLabelText("Выберите маршрут");
    dlg1.setComboBoxItems(routes);
    if (dlg1.exec() == QDialog::Rejected) return;

    query.prepare("SELECT DISTINCT "
                  "tt.id AS id, tt.name AS type "
                  "FROM Transport_On_Routes tor "
                  "JOIN Transports t ON (t.id = tor.transport_id) "
                  "JOIN Transport_Models tm ON (tm.id = t.model_id) "
                  "JOIN Transport_Types tt ON (tt.id = tm.type_id) "
                  "WHERE tor.route_id = :id "
                  "ORDER BY tt.name"
                  );
    query.bindValue(":id", dlg1.currentData().toInt());
    query.exec();

    QMap<int, QString> types;
    while (query.next()) {
        types.insert(query.value("id").toInt(),
                     query.value("type").toString()
                     );
    }

    ComboBoxDialog dlg2;
    dlg2.setWindowTitle("Введите значение");
    dlg2.setLabelText("Выберите ТС");
    dlg2.setComboBoxItems(types);
    if (dlg2.exec() == QDialog::Rejected) return;

    query.prepare("SELECT DISTINCT "
                  "tor.id, tor.transport_id, tor.route_id, tor.depart "
                  "FROM Transport_On_Routes tor "
                  "JOIN Transports t ON (t.id = tor.transport_id) "
                  "JOIN Transport_Models tm ON (tm.id = t.model_id) "
                  "JOIN Transport_Types tt ON (tt.id = tm.type_id) "
                  "JOIN Passengers_Amount pa ON (pa.tor_id = tor_id) "
                  "WHERE tor.route_id = :route_id AND tt.id = :type_id "
                  "ORDER BY tor.depart"
                  );
    query.bindValue(":route_id", dlg1.currentData());
    query.bindValue(":type_id", dlg2.currentData());
    query.exec();

    QList<int> times;
    while (query.next()) {
        times.append(QTime(0, 0, 0).secsTo(query.value("depart").toTime()));
    }

    for (int i = times.size()-1; i > 0; i--) {
        times[i] -= times[i-1];
    }
    times[0] = 0;

    double average = 0;
    foreach (int time, times) {
        average += time;
    }
    average /= (times.size()-1);
    average /= 60;

    QMessageBox::information(this,
                             "Среднее время ожидания",
                             QString::number(average, 'f', 0) + " минут"
                             );
}

void Widget::ways()
{
    QSqlQuery query;
    query.exec("SELECT tt.id, tt.name AS type FROM transport_types tt ORDER BY type");

    QMap<int, QString> types;
    while(query.next()) {
        types.insert(query.value("id").toInt(), query.value("type").toString());
    }

    ComboBoxDialog dlg;
    dlg.setWindowTitle("Введите значение");
    dlg.setLabelText("Выберите ТС");
    dlg.setComboBoxItems(types);
    if (dlg.exec() == QDialog::Rejected) return;

    QDialog wgt;
    QVBoxLayout layout;
    QTableView view;
    QSqlQueryModel model;

    layout.addWidget(&view);
    wgt.setWindowTitle("Маршруты ТС");
    wgt.setLayout(&layout);

    view.setModel(&model);
    model.setQuery("WITH Route_Length AS ( "
                   "SELECT "
                   "r.id, r.number AS route, d.name AS direction, "
                   "SUM(rs.length) AS path "
                   "FROM Routes r "
                   "JOIN Route_Stops rs ON (rs.route_id = r.id) "
                   "JOIN Directions d ON (d.id = r.direction_id) "
                   "GROUP BY r.id, route, direction "
                   ") "
                   "SELECT DISTINCT "
                   "tt.name AS \"Тип ТС\", rl.route AS \"Маршрут\", "
                   "rl.direction AS \"Направление\", rl.path AS \"Протяженность\" "
                   "FROM Route_Length rl "
                   "JOIN Transport_On_routes tor on (tor.route_id = rl.id) "
                   "JOIN Transports t ON (t.id = tor.transport_id) "
                   "JOIN Transport_Models tm ON (tm.id = t.model_id) "
                   "JOIN Transport_Types tt ON (tt.id = tm.type_id) "
                   "WHERE tt.id = '"+dlg.currentData().toString()+"' "
                   "ORDER BY rl.path DESC"
                   );
    view.resizeColumnsToContents();
    view.resizeRowsToContents();

    wgt.resize(view.width(), view.height());
    wgt.exec();
}

void Widget::income()
{
    QDialog wgt;
    QVBoxLayout layout;
    QTableView view;
    QSqlQueryModel model;

    layout.addWidget(&view);
    wgt.setWindowTitle("Поступления по видам ТС");
    wgt.setLayout(&layout);

    view.setModel(&model);
    model.setQuery("SELECT "
                   "tt.name AS \"Тип ТС\", SUM(r.price * pa.amount) AS \"Приход\" "
                   "FROM Transport_On_Routes tor "
                   "JOIN Routes r ON (r.id = tor.route_id) "
                   "JOIN Transports t ON (t.id = tor.transport_id) "
                   "JOIN Transport_Models tm ON (tm.id = t.model_id) "
                   "JOIN Transport_Types tt ON (tt.id = tm.type_id) "
                   "JOIN Passengers_Amount pa ON (pa.tor_id = tor_id) "
                   "GROUP BY tt.name "
                   "ORDER BY tt.name "
                   );
    view.resizeColumnsToContents();
    view.resizeRowsToContents();

    wgt.resize(view.width(), view.height());
    wgt.exec();
}
