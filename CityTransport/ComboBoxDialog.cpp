#include "ComboBoxDialog.h"

#include <QPushButton>
#include <QLayout>

ComboBoxDialog::ComboBoxDialog(QWidget *parent)
    :QDialog(parent)
{
    m_cbbItems = new QComboBox;
    m_lblLabel = new QLabel;

    QPushButton *btnOk = new QPushButton("&Ок");
    QPushButton *btnCancel = new QPushButton("&Отмена");
    btnOk->setDefault(true);

    connect(btnOk, SIGNAL(clicked()), SLOT(accept()));
    connect(btnCancel, SIGNAL(clicked()), SLOT(reject()));

    // Layout setup
    QVBoxLayout *layout = new QVBoxLayout;
    QHBoxLayout *btnLayout = new QHBoxLayout;

    btnLayout->addStretch();
    btnLayout->addWidget(btnOk);
    btnLayout->addWidget(btnCancel);
    btnLayout->addStretch();

    layout->addWidget(m_lblLabel);
    layout->addWidget(m_cbbItems);
    layout->addLayout(btnLayout);

    setLayout(layout);
}

void ComboBoxDialog::setComboBoxItems(const QMap<int, QString> &items)
{
    m_cbbItems->clear();
    foreach (int key, items.keys()) {
        m_cbbItems->addItem(items.value(key), key);
    }
}

void ComboBoxDialog::setLabelText(const QString &text)
{
    m_lblLabel->setText(text);
}

int ComboBoxDialog::currentIndex()
{
    return m_cbbItems->currentIndex();
}

QVariant ComboBoxDialog::currentData()
{
    return m_cbbItems->currentData();
}

QString ComboBoxDialog::currentText()
{
    return m_cbbItems->currentText();
}
