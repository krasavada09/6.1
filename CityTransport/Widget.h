#ifndef WIDGET_H
#define WIDGET_H

#include <QtWidgets>
#include <QtSql>

#include "ComboBoxDialog.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);

private slots:
    void path();
    void time();
    void ways();
    void income();
};
#endif // WIDGET_H
