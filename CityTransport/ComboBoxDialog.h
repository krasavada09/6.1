#ifndef COMBOBOXDIALOG_H
#define COMBOBOXDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QLabel>

class ComboBoxDialog : public QDialog
{
    Q_OBJECT

private:
    QComboBox *m_cbbItems;
    QLabel *m_lblLabel;

public:
    ComboBoxDialog(QWidget *parent = nullptr);

    int currentIndex();
    QVariant currentData();
    QString currentText();

public slots:
    void setComboBoxItems(const QMap<int, QString> &);
    void setLabelText(const QString &);
};

#endif // COMBOBOXDIALOG_H
