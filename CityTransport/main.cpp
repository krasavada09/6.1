#include "Widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Widget w;

    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("bus_station");
    db.setUserName("postgres");
    db.setPassword("example");

    if (!db.open()) {
        QMessageBox::critical(&w,
                              "Ошибка подключения",
                              "Не удалось подключиться к серверу базы данных"
                              );
        return -1;
    }

    w.show();

    return a.exec();
}
